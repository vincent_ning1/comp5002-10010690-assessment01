﻿using System;

namespace XIHA
{
    class Program
    {
        static void Main(string[] args)
        
        {
           //Start the program with Clear();
           Console.Clear();
           Console.WriteLine("============================================");
           Console.WriteLine("============================================");
           Console.WriteLine("============ Welcome to XIHA ==============="); //Print " Welcome to XIHA "
           Console.WriteLine("============================================");
           Console.WriteLine("============================================");
          
          
           var customerName ="";
           double itemPrice;
           var gstPercentage=0.15;
           double totalPrice;  
           var guestAnser="";//Declare var  customerName ,itemPrice ,totalPrice,confirm,guestAnser,gstPercentage=0.15



           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");

           Console.WriteLine("What's your name?");   //Print "What's your name?"
           customerName=(Console.ReadLine());   //Read input from user as customerName
           Console.Clear();
           Console.WriteLine($"hello,"+customerName);    //Print "Hello"+ customerName
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("Please type in a number with 2 decimal places ");//Print "Please type in a number with 2 decimal places "
           itemPrice =double.Parse(Console.ReadLine()); //Read input from user as itemPrice 
         
           Console.WriteLine("Do u want to add something else?(Y for yes, N for no)");                    //print "Do u want to add something else?(Y for yes, N for no)"
           guestAnser=(Console.ReadLine());                                 //Read () as guestAnser
           
           if ( guestAnser is "Y" )                                //if ( guestAnser is Y)
           {                                                        //{
                    Console.WriteLine("Please type in another 2 decimal places number.");//print "Please type in another 2 decimal places number."
                    totalPrice=double.Parse(Console.ReadLine());                         //Read()as totalPrice
                    totalPrice = totalPrice+itemPrice;                                    // add two numbers together( totalPrice = totalPrice+itemPrice)
                    totalPrice=totalPrice*gstPercentage+totalPrice;                     // add GST into the totalPrice (totalPrice=totalPrice*gstPercentage+totalPrice)
           }                                                               //}
           else                                                            //else
           {                                                                //{
               totalPrice=itemPrice+0;                                     //add nothing  (totalPrice=itemPrice+0)
               totalPrice=totalPrice*gstPercentage+totalPrice;             // add GST into the totalPrice ( totalPrice=totalPrice*gstPercentage+totalPrice)
           }                                                                  //}
           Console.WriteLine($"your total spending with GST is"+ totalPrice);//print"your total spending with GST is"+ totalPrice



           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("");
           Console.WriteLine("****************************************************************");
           Console.WriteLine("****************************************************************");
           Console.WriteLine("********Thank you for shopping with us, please come again*******");//print" Thank you for shopping with us, please come again"
           Console.WriteLine("****************************************************************");
           Console.WriteLine("****************************************************************");
           Console.WriteLine("");
           Console.WriteLine("");
        
           

           
           
           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();
        }
    }
}
